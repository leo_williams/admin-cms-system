<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
*@var Illuminate\Routing\Router $router
*/
// might use this for landing page
// Route::get('/', 'WelcomeController@index');

/**
* Load the admin page
*/
Route::get('/', 'Admin\AdminController@adminIndex');

/**
* Load the project files
*/
Route::post('project', 'Admin\AdminController@loadProjectFiles');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

