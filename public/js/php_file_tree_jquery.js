$(document).ready( function() {
	
	// Hide all subfolders at startup
	$(".php-file-tree").find("ul").hide();
	// Expand/collapse on click
	$(".pft-directory A").click( function() {
		$(this).parent().find("UL:first").slideToggle("medium");
		if( $(this).parent().attr('className') == "pft-directory" ) return false;
	});

});
